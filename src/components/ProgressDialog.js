import React from 'react';
import { Modal, View, StyleSheet, Text, ActivityIndicator } from 'react-native';

const ProgressDialog = ({ visible }) => {
  const {
    container,
    content,
    title,
    loading,
    loader,
    loadingContent
  } = styles;

  return (
    <Modal onRequestClose={() => {}}visible={visible}>
      <View style={container}>
        <View style={content}>
          <Text style={title}>Please Wait</Text>
          <View style={loading}>
            <View style={loader}>
              <ActivityIndicator size="small" />
            </View>
            <View style={loadingContent}>
              <Text>Loading</Text>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, .5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    padding: 35,
    backgroundColor: 'white'
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  loading: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  loader: {
    flex: 1,
  },
  loadingContent: {
    flex: 3,
    fontSize: 16,
    paddingHorizontal: 10,
  }
});

export default ProgressDialog;
